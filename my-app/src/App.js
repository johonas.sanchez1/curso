import './App.css';
import Calculadora from './calculadora';
import { ErrorBoundary } from './comunes';
import Demos from './ejemplos';
import Formulario from './formulario';
import Gallery from './muroimagenes/containers/Gallery';
import FotoMuro from './muro'
import PersonasMnt from './personas';


export default function App() {
  return (
    <BrowserRouter>
    <nav></nav>
    <div className='container-fluid'>
      <ErrorBoundary>
        <Routes>
          <Route path='/' element={<Demos />} />
          <Route path='/chisme/de/hacer/numeros' element= {<Calculadora />} />
          <Route path='/formularios' element= {<Formulario />} />
          <Route path='*' element= {<PageNotFound />} />
        </Routes>
        {/* <../calculadora> */}
        <formulario />
        {/* <FotoMuro/> */}
        {/* <Gallery/>  */}
        {/* <PersonasMnt/> */}
      </ErrorBoundary>
    </div>
    </BrowserRouter>
  )
}

// export default App;
