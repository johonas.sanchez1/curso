import React from "react";
import "primereact/resources/themes/saga-blue/theme.css";
import "primereact/resources/primereact.min.css";                 
import "primeicons/primeicons.css";
import "primeflex/primeflex.css";
import logo from "./logo.svg";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { Principal, MainHeader } from "./components/principal/principal";
import { Promotor } from "./components/promotor/promotor";
import { MenuGestor } from "./components/gestor/gestor";

function App() {
  return (
    <>
      <Router>
        <MainHeader />
        <Routes>
          <Route path="/" element={<Principal />}></Route>
          <Route path="/inicio" element={<Principal />}></Route>
          <Route path="/administracion" element={<MenuGestor />}></Route>
          <Route path="/promotor" element={<Promotor />}></Route>
        </Routes>
      </Router>
    </>
  );
}

export default App;
