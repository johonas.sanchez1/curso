import React, { useState, useEffect} from 'react';
import { useForm, Controller } from 'react-hook-form';
import { Dropdown } from 'primereact/dropdown';
import { Skeleton } from 'primereact/skeleton';
import { InputText } from 'primereact/inputtext';
import { InputTextarea } from 'primereact/inputtextarea';
import { Calendar } from 'primereact/calendar';
import { CountryService } from './CountryService';
import { InputNumber } from 'primereact/inputnumber';


export const Promotor = () => {
    const [titulo, setTitulo] = useState('');
    const [nombre, setNombre] = useState('');
    const [descripcion, setDescripcion] = useState('');
    const [pais, setPais] = useState('');
    const [coste, setCoste] = useState('');
    const [categoria, setCategoria] = useState('null');
    const [countries, setCountries] = useState([]);
    const countryservice = new CountryService();
    const [foto, setFoto] = useState('');
    const { control} = useForm({});
    const category = [
        { name: 'Viajes'},
        { name: 'Inmobiliaria'},
        { name: 'Ocio'},
        { name: 'Tecnologia'},
        { name: 'Vehiculos'},
        { name: 'Estudios'}
    ];



    const onCategoria = (e) => {
        setCategoria(e.value);
    }

    
    return (
        <div className="flex align-items-center justify-content-center">
        <div className='surface-card p-4 shadow-2 border-round w-full lg:w-6'>
            <div className="text-center mb-5">
                <h5>Título del proyecto</h5>
                <InputText value={titulo} onChange={(e) => setTitulo(e.target.value)} />
                <span className="ml-2">{titulo}</span>

                <h5>Nombre y apellidos del promotor</h5>
                <InputText value={nombre} onChange={(e) => setNombre(e.target.value)} />
                <span className="ml-2">{nombre}</span>

                <h5>Descripción</h5>
                <span className="p-input-icon-left">
                <InputTextarea value={descripcion} onChange={(e) => setDescripcion(e.target.value)} rows={5} cols={30} autoResize />
                </span>


                {/* <div className="field">
                            <span className="p-float-label">
                                <Dropdown id="country" name="country" value={formik.values.country} onChange={formik.handleChange} options={countries} optionLabel="name" />
                                <label htmlFor="country">Country</label>
                            </span>
                        </div> */}


                <h5>País</h5>
                <span className="p-input-icon-left">
                    <InputText value={pais} onChange={(e) => setPais(e.target.value)}  />
                </span>

                <h5>Coste</h5>
                <div className="text-center mb-5">
                        <label htmlFor="horizontal"></label>
                        <InputNumber inputId="horizontal" value={coste} onValueChange={(e) => setCoste(e.value)} showButtons buttonLayout="horizontal" step={50} min="0" 
                            decrementButtonClassName="p-button-danger" incrementButtonClassName="p-button-success" incrementButtonIcon="pi pi-plus" decrementButtonIcon="pi pi-minus" mode="currency" currency="EUR"/>
                </div>

                {/* <h5>Importe Actual</h5>
                <span className="p-input-icon-left">
                    <InputText value={value3} onChange={(e) => setValue3(e.target.value)}  />
                </span>
                 */}

                <h5>Fecha Inicial Inversión</h5>
                <span className="p-float-label">
                                <Controller name="Fecha Inicial Inversión" control={control} render={({ field }) => (
                                    <Calendar id={field.name} value={field.value} onChange={(e) => field.onChange(e.value)} dateFormat="dd/mm/yy" mask="99/99/9999" showIcon />
                                )} />
                                <label htmlFor="Fecha Inicial Inversión"></label>
                </span>
                
                <h5>Fecha Final Inversión</h5>
                <span className="p-float-label">
                                <Controller name="Fecha Final Inversión" control={control} render={({ field }) => (
                                    <Calendar id={field.name} value={field.value} onChange={(e) => field.onChange(e.value)} dateFormat="dd/mm/yy" mask="99/99/9999" showIcon />
                                )} />
                                <label htmlFor="Fecha Final Inversión"></label>
                </span>
                

                <div className="card">
                <h5>Categoría de Proyecto</h5>
                <Dropdown value={categoria} options={category} onChange={onCategoria} optionLabel="name" placeholder="Selecciona una categoría de proyecto" />
                </div>


                <h5>Foto</h5>
                <InputText value={foto} disabled />

            </div>
        </div>
        </div>
)}