import React from "react";
import { Menubar } from "primereact/menubar";
import { Button } from "primereact/button";
import dreaming from "../../assets/dreaming.jpg";
import { useNavigate } from "react-router-dom";

export function Principal(props) {
  return (
    <>
      <div className="grid grid-nogutter surface-0 text-800">
        <div className="col-12 md:col-6 p-6 text-center md:text-left flex align-items-center ">
          <section>
            <span className="block text-6xl font-bold mb-1">
              Transforma tu dinero
            </span>
            <div className="text-6xl text-primary font-bold mb-3">
              en sueños
            </div>
            <p className="mt-0 mb-4 text-700 line-height-3">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua.
            </p>

            <Button
              label="Learn More"
              type="button"
              className="mr-3 p-button-raised"
            />
            <Button
              label="Live Demo"
              type="button"
              className="p-button-outlined"
            />
          </section>
        </div>
        <div className="col-12 md:col-6 overflow-hidden">
          <img
            src={dreaming}
            alt="hero-1"
            className="md:ml-auto block w-full sm:h-30rem sm:w-auto"
            style={{ clipPath: "polygon(8% 0, 100% 0%, 100% 100%, 0 100%)" }}
          />
        </div>
      </div>
    </>
  );
}

export function MainHeader(props) {
    const navigate = useNavigate();

  const items = [
    {
      label: "Inicio",
      icon: "pi pi-fw pi-home",
      command: ()=>{navigate("/inicio")}
    },
    {
      label: "Users",
      icon: "pi pi-fw pi-user",
      items: [
        {
          label: "New",
          icon: "pi pi-fw pi-user-plus",
        },
        {
          label: "Delete",
          icon: "pi pi-fw pi-user-minus",
        },
        {
          label: "Search",
          icon: "pi pi-fw pi-users",
          items: [
            {
              label: "Filter",
              icon: "pi pi-fw pi-filter",
              items: [
                {
                  label: "Print",
                  icon: "pi pi-fw pi-print",
                },
              ],
            },
            {
              icon: "pi pi-fw pi-bars",
              label: "List",
            },
          ],
        },
      ],
    },
    {
      label: "Events",
      icon: "pi pi-fw pi-calendar",
      items: [
        {
          label: "Edit",
          icon: "pi pi-fw pi-pencil",
          items: [
            {
              label: "Save",
              icon: "pi pi-fw pi-calendar-plus",
            },
            {
              label: "Delete",
              icon: "pi pi-fw pi-calendar-minus",
            },
          ],
        },
        {
          label: "Archieve",
          icon: "pi pi-fw pi-calendar-times",
          items: [
            {
              label: "Remove",
              icon: "pi pi-fw pi-calendar-minus",
            },
          ],
        },
      ],
    },
    {
      label: "Quit",
      icon: "pi pi-fw pi-power-off",
    },
    {
      label: "Area de Promotor",
      icon: "pi pi-fw pi-briefcase",
      command: () => {navigate("/promotor")}
    },
    {
      label: "Area de Inversor",
      icon: "pi pi-fw pi-briefcase",
      // command: () => {navigate("/inversor")}
    },
    {
      label: "Administración",
      icon: "pi pi-fw pi-cog",
      command: ()=>{navigate("/administracion")} ,
    },
  ];

  const start = (
    <img
      alt="logo"
      src="/public/logo192.png"
      onError={(e) =>
        (e.target.src =
          "https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png")
      }
      height="40"
      className="mr-2"
    ></img>
  );
  const end = "";

  return (
    <div>
      <div className="card">
        <Menubar model={items} start={start} end={end} />
      </div>
    </div>
  );
}
